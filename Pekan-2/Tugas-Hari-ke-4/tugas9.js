// Soal 1
// Ini versi ES5
/*const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName()
*/

// Ini versi ES6
const literal = (firstName,lastName) => {
  fullName = () => {
    console.log(firstName+' '+lastName)
  }
  return fullName()
}

const newFunction = literal
newFunction('William', 'Imoh')
console.log()

// Soal 2

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName,lastName,destination,occupation,spell} = newObject
console.log(firstName, lastName, destination, occupation)
console.log()

// Soal 3
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined2 = west.concat(east)

const combined = [...west,...east]
console.log(combined)