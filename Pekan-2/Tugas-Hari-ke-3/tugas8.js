// Soal 1
// Luas lingkaran
let pi = 3.14
console.log('Luas lingkaran = pi x r x r')
const luasLingk = (r) => {
    //function
    return  `Luas lingkaran = ${pi*r*r}`
        //returning a function
    
}
console.log(luasLingk(10));
console.log();

// Keliling lingkaran
console.log('Keliling lingkaran = 2 x pi x r')
const keliLingk = (r) => {
    return `Keliling lingkaran = ${2*pi*r}`
}
console.log(keliLingk(50))
console.log()

// Soal 2
const kataPertama = 'saya'
const kataKedua = 'adalah'
const kataKetiga = 'seorang'
const kataKeempat = 'frontend'
const kataKelima = 'developer'

let kalimat = []

const fungsiAdd = (kataPertama,kataKedua,kataKetiga,kataKeempat,kataKelima) => {
    return kataPertama+kataKedua+kataKetiga+kataKeempat+kataKelima;
}

kalimat.push(fungsiAdd())
console.log(kalimat)
console.log()

// Soal 3
class Book {
    constructor(name, totalPage, price){
        this._name = name;
        this._totalPage = totalPage;
        this._price = price;
    }
    get name(){
        return this._name;
    }
    get totalPage(){
        return this._totalPage;
    }
    get price(){
        return this._price;
    }
    set price(harga){
        return this._price = harga
    }
}
 
const buku = new Book("Laskar Pelangi", 200, 50000);
 
console.log(buku.name) ;
console.log(buku.totalPage);
console.log(buku.price);

console.log();

class Comic extends Book {
    constructor(name, harga, isColorful){
        super(name)
        this._price = harga
        this._isColorful = isColorful;
    }
    get name(){
        return this._name
    }
    get isColorful(){
        return this._isColorful
    }
    
}

const komik = new Comic('Doraemon',700000,true);

console.log(komik.name)
console.log(komik.price)
console.log(komik.isColorful)
