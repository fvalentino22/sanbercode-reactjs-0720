// Soal 1
var arrayDaftarPeserta = ["Asep", "laki - laki", "baca buku", 1992]

var arrayDaftarPeserta = {
    nama : "Asep",
    "jenis kelamin" : "laki - laki",
    hobi : "baca buku",
    "tahun lahir" : 1992

}

console.log(arrayDaftarPeserta["tahun lahir"]);
console.log(arrayDaftarPeserta.hobi);
console.log();

// Soal 2
var buah = [
    {
        nama: "strawberry", 
        warna: "merah", 
        "ada bijinya": "tidak",
        harga: 9000
    }, 
    {
        nama: "jeruk", 
        warna: "oranye", 
        "ada bijinya": "ada",
        harga: 8000
    }, 
    {
        nama: "semangka", 
        warna: "hijau & merah", 
        "ada bijinya": "ada",
        harga: 10000
    },
    {
        nama: "pisang", 
        warna: "kuning", 
        "ada bijinya": "tidak",
        harga: 5000
    }
]

buah.forEach(function(dataBuah){
    console.log("nama : " + dataBuah.nama);
 })
 console.log();

 // Soal 3
 var dataFilm = []

 function fungsiFilm(nama, durasi, genre, tahun) {
    var itemFilm = { 
        nama : nama,
        durasi : durasi,
        genre : genre,
        tahun : tahun,
    }
    dataFilm.push(itemFilm)
    console.log('Data Film = ',dataFilm)
}

fungsiFilm("Joker","122 menit","Crime, Drama, Thriller",2019);
console.log();

// Soal 4
// Release 0
class Animal {
    constructor(name){
        this.name = name;
    }
    get legs() {
        return 4;
    }
    get cold_blooded() {
        return false;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) ;
console.log(sheep.legs) ;
console.log(sheep.cold_blooded) ;
console.log();

// Release 1
class Ape extends Animal {
    constructor(cold_blooded){
        super(cold_blooded);
    }
    nama(){
        return this.nama = name;
    }
    get legs() {
        return 2;
    }
    yell(){
        console.log("Auooo");
    } 
}

class Frog extends Animal {
    constructor(legs){
        super(legs);
    }
    coldblood(){
        this.cold_blooded = cold_blooded;
    }
    nama(){
        this._name = name;
    }       
    jump(){
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
//console.log(sungokong.name)
//console.log(sungokong.legs)
//console.log(sungokong.cold_blooded)
sungokong.yell()


var kodok = new Frog("buduk")
//console.log(kodok.name)
//console.log(kodok.legs)
//console.log(kodok.cold_blooded)
kodok.jump()
console.log()


// Soal 5
class Clock {
    constructor(jam, menit, detik){
        this.jam = jam;
        this.menit = menit;
        this.detik = detik;
    }
    render(){
        
    }
    stop(){
        clearInterval(timer);
    }
    start(){
        render();
        timer = setInterval(render, 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();
