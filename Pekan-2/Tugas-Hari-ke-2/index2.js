var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function recursive(time,i){
    readBooksPromise(time,books[i].then(function(sisaWaktu){
        if(sisaWaktu > 0){
            (function(baca){
                if(i + 1 < books.length){
                    recursive(sisaWaktu,i+1)
                }
                else{
                    console.log('semua buku sudah dibaca')
                }
            })
        }
    })
    )}