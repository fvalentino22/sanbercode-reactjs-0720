// Latihan 1 
var sayHello = "Hello World!"
console.log(sayHello)

//Latihan 2 --> Variable Number/String/Boolean
var name = "John"
var angka = 12
var todayIsFriday = false
var items

console.log(name)
console.log(angka)
console.log(todayIsFriday)
console.log(items)

// Latihan 3 --> String index
var sentences = "Javascript"
console.log(sentences[0])
console.log(sentences[2])

// Latihan 4 --> string length
var word = "Javascript is awesome"
console.log(word.length)

//Latihan 5 --> character index
console.log('i am a string'.charAt(3))

// Latihan 6 --> concate string
var string1 = 'good'
var string2 = 'luck'

console.log(string1.concat(string2))

// Latihan 7 --> index of
var text = 'dung dung!'
console.log(text.indexOf('dung'))
console.log(text.indexOf('u'))

// Latihan 8 --> substring
var car1 = 'Lykan Hypersport'
var car2 = car1.substring(4)
console.log(car2)

// Latihan 9 --> substring
var motor1 = 'zelda motor'
var motor2 = motor1.substr(2,6)
console.log(motor2)

// Latihan 10 --> to upper case
var letter = 'This Letter Is For You'
var upper = letter.toUpperCase()
console.log(upper)

// Latihan 11 --> to lower case
var letter = 'This Letter Is For You'
var lower = letter.toLowerCase()
console.log(lower)

// Latihan 12 --> trim
var username = 'administrator'
var newUsername = username.trim()
console.log(newUsername)