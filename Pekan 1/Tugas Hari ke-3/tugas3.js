// Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var newkataKedua = kataKedua.charAt(0).toUpperCase();
var kataKedua1 = kataKedua.substring(1);
var newkataKeempat = kataKeempat.toUpperCase();
var spasi = " ";

var hasil1 = kataPertama.concat(spasi);
var hasil2 = newkataKedua.concat(kataKedua1);
var hasil3 = spasi.concat(kataKetiga);
var hasil4 = spasi.concat(newkataKeempat);
var hasil5 = hasil1.concat(hasil2);
var hasil6 = hasil3.concat(hasil4);
var hasilfix = hasil5.concat(hasil6);

console.log(hasilfix);
console.log();

// Soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var newkataPertama = Number("1");
var newkataKedua = Number("2");
var newkataKetiga = Number("4");
var newkataKeempat = Number("5");

var hasil = newkataPertama+newkataKedua+newkataKetiga+newkataKeempat
console.log(hasil);
console.log();

// Soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4,10); // do your own! 
var kataKetiga = kalimat.substr(15,3); // do your own! 
var kataKeempat = kalimat.substr(19,5); // do your own! 
var kataKelima = kalimat.substr(25); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
console.log();

// Soal 4
var nilai = 60;
if (nilai >= 80) {
    console.log("indeksnya A")
}else if (nilai >= 70 && nilai < 80) {
    console.log("indeksnya B") 
}else if (nilai >= 60 && nilai < 70) {
    console.log("indeksnya C")
}else if (nilai >=50 && nilai < 60) {
    console.log("indeksnya D")
}else {
    console.log("indeksnya E")
}
console.log();

// Soal 5
var tanggal = 22;
var bulan = 3;
var tahun = 1997;
switch(bulan) {
    case 1: {console.log(tanggal+' Januari '+tahun); break;}
    case 2: {console.log(tanggal+' Februari '+tahun); break;}
    case 3: {console.log(tanggal+' Maret '+tahun); break;}
    case 4: {console.log(tanggal+' April '+tahun); break;}
    case 5: {console.log(tanggal+' Mei '+tahun); break;}
    case 6: {console.log(tanggal+' Juni '+tahun); break;}
    case 7: {console.log(tanggal+' Juli '+tahun); break;}
    case 8: {console.log(tanggal+' Agustus '+tahun); break;}
    case 9: {console.log(tanggal+' September '+tahun); break;}
    case 10: {console.log(tanggal+' Oktober '+tahun); break;}
    case 11: {console.log(tanggal+' November '+tahun); break;}
    case 12: {console.log(tanggal+' Desember '+tahun); break;}

    console.log();
}