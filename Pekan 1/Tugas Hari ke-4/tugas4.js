// Soal 1
console.log('LOOPING PERTAMA')
var deret = 0;
while(deret < 20){
    deret += 2;
    console.log(deret+' - I love coding ')
}

console.log('LOOPING KEDUA')
var deret = 22;
while(deret > 2 ){
    deret -= 2;
    console.log(deret+' - I love coding ')
}

console.log();

// Soal 2
for (var angka = 1; angka < 21; angka++) {
    if(angka % 2 == 0){
        console.log(angka+' - Santai');
    }
    else if(angka % 3 == 0){
        console.log(angka+' - I love coding');
    }
    else{
        console.log(angka+' - Berkualitas');
    }
}
console.log('');
 
// Soal 3
var baris;
var kolom;
for(baris = 1; baris <= 7; baris++){
    var string = '#'
    for(kolom = 1; kolom < baris; kolom++){
        string = string + '#'
    }
    console.log(string);
}
console.log();

// Soal 4
var kalimat = "saya sangat senang belajar javascript";
var kalimatArr = kalimat.split(" ");
kalimatArr.splice(1,4,kalimatArr[1],kalimatArr[2],kalimatArr[3],kalimatArr[4])
console.log(kalimatArr);
console.log();

// Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
console.log(daftarBuah[0]);
console.log(daftarBuah[1]);
console.log(daftarBuah[2]);
console.log(daftarBuah[3]);
console.log(daftarBuah[4]);

console.log()

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
for (var jml = 0; jml < 5; jml++) {
    console.log(daftarBuah[jml]);
}